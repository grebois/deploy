FROM alpine
RUN echo "https://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories \
    && apk update && apk add --update --no-cache terraform kubectl kustomize helm flux
